_Requires Go 1.17._

## Test

```
make test
```

## Build

```
make build
```

## Run

```
./mds 1972.78
```

## Thoughts

Suppose we extend the `money` package to allow for different coin sets. If the
coins were 1, 3, and 4, our minimal denomination set would be (4, 1, 1) but the 
optimal solution is in fact (3, 3).

The coin system given, which most currencies use, do not have this problem. That
is because they satisfy the theorem given [here](https://math.stackexchange.com/a/1209657).

Another solution would be needed to find the optimal solution in cases which don't
satisfy the theorem given above, e.g. with dynamic programming.
