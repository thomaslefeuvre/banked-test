package main

import (
	"fmt"
	"os"
	"strconv"

	"github.com/thomaslefeuvre/banked-test/money"
)

func main() {
	args := os.Args[1:]
	if len(args) == 0 {
		usage()
		os.Exit(1)
	}

	f, err := strconv.ParseFloat(args[0], 64)
	if err != nil {
		usage(fmt.Errorf("%s is not a valid float", args[0]))
		os.Exit(1)
	}

	amount := money.New(f)

	mds, err := money.MinimalDenominationSet(amount)
	if err != nil {
		usage(err)
		os.Exit(1)
	}

	fmt.Printf("minimal denomination set for %s:\n%s", amount, mds)
}

func usage(errs ...error) {
	fmt.Printf("Usage: %s <amount: float>\n", os.Args[0])
	for _, err := range errs {
		fmt.Printf("error: %s\n", err)
	}
}
