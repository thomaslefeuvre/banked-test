package money

import (
	"errors"
	"fmt"
)

// units represents the denominations of money
// available in the currency, in descending order.
var units = []Money{
	New(500.0),
	New(200.0),
	New(100.0),
	New(50.0),
	New(20.0),
	New(10.0),
	New(5.0),
	New(2.0),
	New(1.0),
	New(0.5),
	New(0.2),
	New(0.1),
	New(0.05),
	New(0.02),
	New(0.01),
}

// Money represents a quantity of money in
// the currency being used.
type Money int64

// New creates a new quantity of money from
// a float value. The currency can be divided
// into hundredths, so we store the integer
// quantity of the smallest denomination of the
// currency.
func New(m float64) Money {
	return Money((m * 100) + 0.5)
}

// Float64 converts the Money type back to
// its quantity in float64.
func (m Money) Float64() float64 {
	x := float64(m)
	x = x / 100
	return x
}

// String prints the money quantity to two
// decimal places (so all possible values can
// be represented.
func (m Money) String() string {
	return fmt.Sprintf("%.2f", m.Float64())
}

// Collection represents a collection of
// money units. The underlying type is a
// map of denominations to their quantities.
type Collection map[Money]int64

// Sum returns the sum of the money
// included in the Collection.
func (c Collection) Sum() Money {
	var sum Money
	for amt, qty := range c {
		sum += Money(int64(amt) * qty)
	}
	return sum
}

// String converts a Collection to a pretty
// string for printing.
func (c Collection) String() string {
	var s string

	if c == nil {
		s = "No coins in collection.\n"
		return s
	}

	for _, u := range units {
		count := c[u]
		if count > 0 {
			s += fmt.Sprintf(" - %d * £%s\n", count, u)
		}
	}

	if s == "" {
		s = "No coins in collection.\n"
	}

	return s
}

// MinimalDenominationSet calculates the money
// units needed such that the quantity of money
// units is minimised. Errors for negative amounts.
func MinimalDenominationSet(amount Money) (Collection, error) {
	if amount < 0 {
		return nil, errors.New("amount is negative")
	}

	result := make(Collection)

	current := amount
	for _, unit := range units {
		n := int64(current / unit)
		if n > 0 {
			result[unit] = n
		}
		current -= Money(n * int64(unit))
	}

	return result, nil
}
