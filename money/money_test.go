package money

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMoney(t *testing.T) {
	assert.Equal(t, Money(100), New(1))
	assert.Equal(t, Money(10), New(0.10))
	assert.Equal(t, Money(1), New(0.01))
	assert.Equal(t, Money(1), New(0.005))
	assert.Equal(t, Money(0), New(0.0049))

	assert.Equal(t, 1.0, Money(100).Float64())
	assert.Equal(t, 0.1, Money(10).Float64())
	assert.Equal(t, 0.01, Money(1).Float64())
	assert.Equal(t, 0.0, Money(0).Float64())

	assert.Equal(t, "1.00", Money(100).String())
	assert.Equal(t, "0.10", Money(10).String())
	assert.Equal(t, "0.01", Money(1).String())
	assert.Equal(t, "0.00", Money(0).String())
}

func TestCollection(t *testing.T) {
	var c Collection
	assert.Equal(t, "No coins in collection.\n", c.String())
	assert.Equal(t, New(0), c.Sum())

	c = make(Collection)
	assert.Equal(t, "No coins in collection.\n", c.String())
	assert.Equal(t, New(0), c.Sum())

	c[New(1)] = 0
	assert.Equal(t, "No coins in collection.\n", c.String())
	assert.Equal(t, New(0), c.Sum())

	c[New(1)] = 1
	assert.Equal(t, " - 1 * £1.00\n", c.String())
	assert.Equal(t, New(1), c.Sum())

	c[New(0.5)] = 2
	assert.Equal(t, " - 1 * £1.00\n - 2 * £0.50\n", c.String())
	assert.Equal(t, New(2), c.Sum())
}

func TestMinimalDenominationSet(t *testing.T) {
	output, err := MinimalDenominationSet(New(0.005))

	assert.NoError(t, err)
	assert.NotEmpty(t, output)
	assert.Equal(t, New(0.01), output.Sum())

	output, err = MinimalDenominationSet(New(0.0049))

	assert.NoError(t, err)
	assert.NotNil(t, output)
	assert.Empty(t, output)
	assert.Equal(t, New(0), output.Sum())

	output, err = MinimalDenominationSet(New(0))

	assert.NoError(t, err)
	assert.NotNil(t, output)
	assert.Empty(t, output)
	assert.Equal(t, New(0), output.Sum())

	output, err = MinimalDenominationSet(New(-1.0))

	assert.Error(t, err)
	assert.Nil(t, output)
	assert.Equal(t, New(0), output.Sum())

	output, err = MinimalDenominationSet(New(1972.78))

	assert.NoError(t, err)
	assert.Len(t, output, 10)
	assert.Equal(t, int64(3), output[New(500)])
	assert.Equal(t, int64(2), output[New(200)])
	assert.Equal(t, int64(1), output[New(50)])
	assert.Equal(t, int64(1), output[New(20)])
	assert.Equal(t, int64(1), output[New(2)])
	assert.Equal(t, int64(1), output[New(0.5)])
	assert.Equal(t, int64(1), output[New(0.2)])
	assert.Equal(t, int64(1), output[New(0.05)])
	assert.Equal(t, int64(1), output[New(0.02)])
	assert.Equal(t, int64(1), output[New(0.01)])
	assert.Equal(t, New(1972.78), output.Sum())
}
